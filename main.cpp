#include <ctime>
#include <cstdlib>
#include <thread>
#include <mutex>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <signal.h>
#include "infofilo.h"
#define MAXESTOMAGO 100
using namespace std;
//vector<mutex> candado;
vector <thread> filosofos;
mutex *candado;
mutex candadoComida;
unsigned long comida = -1;
int restaurar = 0;
int cantFilo;
InfoFilo *info;
bool flag=true;

void manejadorSignal(int signum){
	if (signum == 2){
		flag = false;
		for(int i = 0; i < cantFilo; i++){
			filosofos[i].join();
		}
		cout << "\n#############\n";
		cout <<   "Resumen Total\n";
		cout <<   "#############\n";
		unsigned long comidos = 0;
		for (int i = 0; i < cantFilo; i++){
			cout << "\nFilosofo #" << i << endl;
			cout << "Estomago: " << info[i].estomago << endl;
			cout << "Veces que ha pensado: " << info[i].contPensar << endl;
			cout << "Veces que ha comido:  " << info[i].contComer << endl;
			comidos += info[i].contComer;
		}
	}
	cout << "Senal #" << signum;
	flag = false;
	exit(signum);
}

void filosofo(int nombre){
	//cout<<"creando"<<nombre<<endl<<flush;
	int tenedor2=(nombre+1)%cantFilo;
	//int estomago=10; // porque si inicializa en 0, se moriria
	info[nombre].estomago = 10;
	info[nombre].contPensar = 0;
	info[nombre].contComer = 0;
	while (flag){
		cout<<"Filosofo "<<nombre<<" pensando"<<endl<<flush;
		info[nombre].estomago--; // 6. cada que piensa su estomago se reducira
		info[nombre].contPensar++;
		usleep(rand()%1000000);
		if(info[nombre].estomago >= MAXESTOMAGO || comida == 0){
			continue;
		}
		candado[nombre].lock();
		candado[tenedor2].lock();
		candadoComida.lock();
		if (comida <= 1){
			restaurar++;
			comida = -1;
		}else{
			comida--;
		}
		candadoComida.unlock();
		cout<<"Filosofo "<<nombre<<" comiendo"<<endl<<flush;
		info[nombre].estomago++;
		info[nombre].contComer++;
		usleep(rand()%1000000);
		candado[nombre].unlock();
		candado[tenedor2].unlock();


	}
	cout << "Terminando "<<nombre<<endl;
}
int main(int argc, char* argv[]){
	signal(SIGINT, manejadorSignal);
	srand(time(NULL));
        cout<<"Ingrese la cantidad de filosofos"<<endl;
        cin>>cantFilo;

	//vector<thread> filosofos;
	candado=new mutex[cantFilo];
	info = new InfoFilo[cantFilo];
	int nombres[cantFilo];


	for(int i=0;i<cantFilo;i++){
		nombres[i]=i;
	}



	for(int i =0;i<cantFilo;i++){
		filosofos.push_back(thread(filosofo,nombres[i]));
	       // candado.push_back(mutex());


	}

	for(int i=0;i<cantFilo;i++){
		filosofos[i].join();
	}

	return 0;
}

